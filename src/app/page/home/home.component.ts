import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  title = 'app works!! ====';
  url = 'http://www.google.com';
  @Input() name = '';
  code = '000';
  @Output() formSubmit: EventEmitter<string> = new EventEmitter();
  showContent = true;
  mode = 'Hide';
  list = ['One', 'Two', 'Three'];
  price = 123456.56543;
  currentDate = new Date();

  constructor(private fb: FormBuilder) {}

  simple = this.fb.group({
    code: ['', [Validators.required, Validators.minLength(3)]],
    name: ['', [Validators.maxLength(15)]],
  });

  simpleValue = this.fb.group({
    text: ['', [Validators.required, Validators.maxLength(5)]],
  });

  onSimpleValueSubmit() {
    if (this.simpleValue.valid) {
      const { text } = this.simpleValue.value;
      this.list.push(text);
    } else {
      console.log('Invalid form value');
    }
  }

  ngOnInit(): void {}

  onBtnClick() {
    console.log('on btn click..');
  }

  onSubmit(simpleForm: NgForm) {
    if (simpleForm.valid) {
      console.log('Send data to server');
    } else {
      console.log('show warning');
    }
  }

  onSimpleSubmit() {
    if (this.simple.valid) {
      console.log('Send data to server');
      this.formSubmit.emit(this.simple.get('code')?.value);
    } else {
      console.log('show warning');
    }
  }

  toggle() {
    this.mode = this.showContent ? 'Show' : 'Hide';
    this.showContent = !this.showContent;
  }
}
