import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private router: Router,
    private dialog: MatDialog
  ) {}

  registerForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    code: ['', [Validators.required, Validators.maxLength(5)]],
    name: ['', [Validators.required, Validators.maxLength(150)]],
    pwd: ['', [Validators.required, Validators.maxLength(15)]],
  });

  ngOnInit(): void {}

  onRegisterSubmit() {
    if (this.registerForm.valid) {
      this.userService.createUser(this.registerForm.value).subscribe((resp) => {
        if (resp) {
          this.router.navigate(['', 'login']);
        } else {
          // (2) when register fail
          this.dialog.open(RegisterFailDialog);
        }
      });
    } else {
      // (1) when form invalid
      this.dialog.open(RegisterDialog);
    }
  }
}

@Component({
  selector: 'register-dialog',
  templateUrl: './register-dialog.html',
})
export class RegisterDialog {}

@Component({
  selector: 'register-fail-dialog',
  templateUrl: './register-fail-dialog.html',
})
export class RegisterFailDialog {}
