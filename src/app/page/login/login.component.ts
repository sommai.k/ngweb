import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UserService } from 'src/app/service/user.service';
import {
  RegisterDialog,
  RegisterFailDialog,
} from '../register/register.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private router: Router,
    private dia: MatDialog
  ) {}

  loginForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    pwd: ['', [Validators.required, Validators.maxLength(15)]],
  });

  ngOnInit(): void {}

  onLoginSubmit() {
    if (this.loginForm.valid) {
      this.userService.login(this.loginForm.value).subscribe((resp) => {
        if (resp.success) {
          this.router.navigate(['', 'user']);
        } else {
          // (2) Login Fail
          this.dia.open(RegisterFailDialog);
        }
      });
    } else {
      // (1) Invalid form
      this.dia.open(RegisterDialog);
    }
  }

  gotoRegisterPage() {
    this.router.navigate(['', 'register']);
  }
}
